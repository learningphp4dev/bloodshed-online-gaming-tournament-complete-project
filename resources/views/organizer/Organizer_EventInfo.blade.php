@extends('layout.org_navbar')

@section('navbar')
@parent
@endsection


@section('main_content')
    <title>Document</title>
   
    <link rel="stylesheet" href="{{asset(url('organizer.Organizer_EventInfo.css'))}}">
 
</head>

<body class="bg-dark">

    <!-- Main Navbar -->
   
    <!-- Navbar -->
    <!-- Body Content Start -->
    <div class="container-fluid">
        <div class="row">
            <div class="col-md-12 px-0">
            <header>
                <div class="dashb pt-4 pl-5 text-white text-uppercase text-accent-1 text-center">
                    <h1>Dashboard</h1>
                </div>
                <div class="hamburger">
                    <i class="nav_icon fas fa-bars"></i>
                </div>
                <nav class="sidebar">
                    <!-- profile picture -->
                    <div class="profile_picture">
                        <img src='{{asset(url("$org_detail->username"))}}' alt="">
                    </div>

                    <div class="profile_name">
                        {{Session::get('username')}}
                    </div>
                    <ul class="nav-list">
                        <li class="nav-item">
                            <a href="{{url('organizer_dashboard')}}" class="nav-link current">
                                <i title="Dashboard" class="fas fa-tachometer-alt"></i>Dashboard
                            </a>
                        </li>
                        <li class="nav-item">
                            <a href="{{url('create_event')}}" class="nav-link">
                                <i title="Create Event" class="fas fa-plus-square"></i>Create Event
                            </a>
                        </li>
                        <li class="nav-item">
                            <a href="{{url('check-event')}}" class="nav-link">
                                <i title="Check Event" class="fas fa-check-double"></i>Check Event
                            </a>
                        </li>
                        <li class="nav-item">
                            <a href="{{asset(url('activity'))}}" class="nav-link">
                                <i title="Activity" class="fab fa-readme"></i>Activity
                            </a>
                        </li>
                        <li class="nav-item">
                            <a href="{{asset(url('logout'))}}" class="nav-link">
                                <i title="Logout" class="fas fa-sign-out-alt"></i>Logout
                            </a>
                        </li>
                    </ul>
                    <div class="social-media">
                        <a href="" class="icon-link">
                            <i class="fab fa-instagram"></i>
                        </a>
                        <a href="" cladss="icon-link">
                            <i class="fab fa-twitter-square"></i>
                        </a>
                        <a href="" class="icon-link">
                            <i class="fab fa-linkedin"></i>
                        </a>
                        <a href="" class="icon-link">
                            <i class="fab fa-facebook"></i>
                        </a>
                    </div>
                </nav>

            </header>
                <main>
                    <div class="zoom-content">

                        <section>
                            <!-- 1st container -->
                            <div class="container-fluid p-0 m-0 ">
                                <!-- Page heading -->
                                <div class="text-white mb-4 text-uppercase text-center dashboard_heading">
                                    <h1>Dashboard</h1>
                                </div>
                                <div class="main_heading">
                                    <h1 class="heading text-white text-center p-2">
                                        EVENT DETAIL
                                    </h1>
                                </div>
                            </div>

                            <!-- 2nd container -->
                            <!-- Logo and Banner -->
                            <div class="container-fluid  pt-3 pb-3 p-0 m-0 logo_banner">
                                <div class="row p-0 m-0">
                                    <!-- Logo -->
                                    <div class="logo col-md-6 position-relative">
                                        <h2>GAME LOGO</h2>
                                        <div class="logo_img">
                                            <img src="<?php echo '../' . $data['logo']; ?>" class="rounded img-fluid" alt="Logo">
                                        </div>
                                    </div>
                                    <!-- Banner -->
                                    <div class="banner col-md-6 position-relative">
                                        <h2>GAME BANNER</h2>
                                        <div class="banner_img">
                                            <img src="<?php echo '../' . $data['banner']; ?>" class="rounded d-block w-100" alt="Banner">

                                        </div>

                                    </div>
                                </div>
                            </div>


                            <!-- 3rd container  game info-->
                            <div class="container-fluid third-section p-0 m-0 mt-5">
                                <div class="game_info">
                                    <h1 class="text-center text-white">
                                        BASIC INFO
                                    </h1>
                                </div>
                                <div class="edit_page d-inline-block p-2" id="edit_button">
                                    <img src="https://img.icons8.com/color/48/000000/settings.png" />
                                </div>
                                <article>
                                    <div class="event_info">
                                        <div class="info1">
                                            <div class="head1">Game Name </div>
                                            <div class="body1"><?php echo strtoupper($data['game'] . ' Mobile'); ?></div>
                                        </div>

                                        <div class="info2">
                                            <div class="head1">Tournament Name </div>
                                            <div class="body1"><?php echo strtoupper($data['tournament_name']); ?></div>
                                        </div>
                                        <div class="info3">
                                            <div class="head1">Platform </div>
                                            <div class="body1"><?php echo strtoupper($data['platform']); ?></div>
                                        </div>
                                        <div class="info4">
                                            <div class="head1">Entry Fee </div>
                                            <div class="body1"><?php echo strtoupper($data['price']); ?></div>
                                        </div>
                                        <div class="info5">
                                            <div class="head1">Game Type</div>
                                            <div class="body1"><?php echo strtoupper($data['type']); ?></div>
                                        </div>
                                        <div class="info6">
                                            <div class="head1">Date & Time</div>
                                            <div class="body1"><?php echo $data['start_date'] . ' -> ' . $data['time'] . ' PM'; ?><br>Check-in -> <?php echo $data['check_in']; ?>PM</div>
                                        </div>
                                        <div class="info7">
                                            <div class="head1">Place and Address</div>
                                            <div class="body1"><?php echo $data['address']; ?></div>
                                        </div>
                                        <div class="info8 d-block">
                                            <div id="accordion" class="">
                                                <div class="card">
                                                    <div class="card-header">
                                                        <a href="#prize" data-toggle="collapse" class="card-link d-block">
                                                            <h3>Prize</h3>
                                                        </a>
                                                    </div>
                                                    <div id="prize" class="collapse" data-parent="#accordion">
                                                        <table class="table text-center w-75 mx-auto table-striped">
                                                            <thead>
                                                                <th>Position</th>
                                                                <th>Prize</th>
                                                            </thead>
                                                            <tbody>
                                                                <?php

                                                                $search =  array_search($event_id, array_column(json_decode($prize_file), 'event_id'));
                                                                $ss = $prize_array[$search];
                                                                $i = 1;
                                                                foreach (array_slice($ss, 1) as $key) {
                                                                ?>
                                                                    <tr>
                                                                        <td><?php echo 'Position ' . $i; ?></td>
                                                                        <td><?php echo $key; ?></td>
                                                                    </tr>
                                                                <?php
                                                                    $i++;
                                                                }
                                                                ?>
                                                            </tbody>
                                                        </table>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="info9 d-block">
                                            <div id="accordion" class="">
                                                <div class="card">
                                                    <div class="card-header">
                                                        <a href="#community" data-toggle="collapse" class="card-link d-block">
                                                            <h3>Schedule</h3>
                                                        </a>
                                                    </div>
                                                    <div id="community" class="collapse" data-parent="#accordion">
                                                        <table class="table text-center">
                                                            <thead>
                                                                <th>Time</th>
                                                                <th>Team Limit</th>
                                                            </thead>
                                                            <tbody>
                                                                <?php

                                                                $search =  array_search($event_id, array_column(json_decode($file), 'event_id'));
                                                                $ss = $time_array[$search];
                                                                $i = 1;
                                                                foreach (array_slice($ss, 1) as $key) {
                                                                ?>
                                                                    <tr>
                                                                        <td><?php echo 'Batch ' . $i; ?></td>
                                                                        <td><?php echo $key; ?></td>
                                                                    </tr>
                                                                <?php
                                                                    $i++;
                                                                }
                                                                ?>
                                                            </tbody>
                                                        </table>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </article>
                            </div>
                        </section>
                    </div>
                </main>
            </div>
        </div>
    </div>


    <!-- Javascript -->
    <script src="../JavaScript/Organizer_menu.js"></script>
    <script src="Organizer_Dashboard.js"></script>
    <script>
        // document.getElementById('edit_button').style.position = 'unset';

        // window.onscroll = function () {
        //     var scroll = window.pageYOffset;

        //     console.log(scroll);

        //     if (scroll >= 790) {
        //         document.getElementById('edit_button').style.position = 'fixed';
        //         document.getElementById('edit_button').style.right = '0';
        //     } else {
        //         document.getElementById('edit_button').style.position = 'unset';
        //     }
        // }
    </script>

</body>

</html>