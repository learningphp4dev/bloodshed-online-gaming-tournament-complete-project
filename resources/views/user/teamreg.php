<?php
session_start();
if (isset($_SESSION['username']) == '') {
    header('Location: ../static/index.php');
}
$connect = new mysqli('localhost', 'root', '', 'bloodshed');

if (isset($_POST['submit_team'])) {

    // $data_validation = "SELECT * FROM user_team_info WHERE team_name = "$_POST['team_name']"";
 
    $username = 'Gagandeep';
    $team_name = $_POST['team_name'];
    $role = $_POST['role'];
    $team_size = $_POST['team_size'];
    $platform = $_POST['platform'];
    $game_type = $_POST['game_type'];
    $game_id = $_POST['game_id'];
    $player_one;
    $player_two;
    $player_three;
    $player_four;
    $player_one_id;
    $player_two_id;
    $player_three_id;
    $player_four_id;

    // Image
    $team_image_size = $_FILES['team_image']['size'];
    if ($team_image_size > 102400) {
        echo "sorry file should be less then 100kb";
    } else {
        $name = $_FILES['team_image']['name'];
        $tmpname = $_FILES['team_image']['tmp_name'];
        echo $tmpname;
        $folder = 'user_team_profile/' . $name;
        move_uploaded_file($tmpname, '../user_team_profile/' . $name);
        echo "success";
    }
    // Image

    if ($team_size == 'duo') {
        $player_one = $_POST['player_one_name'];
        $player_one_id = $_POST['player_one_id'];
        $player_two = $_POST['player_two_id'];
        $player_two_id = $_POST['player_two_id'];
    } else if ($team_size == 'squad') {
        $player_one = $_POST['player_one_name'];
        $player_one_id = $_POST['player_one_id'];
        $player_two = $_POST['player_two_id'];
        $player_two_id = $_POST['player_two_id'];
        $player_three = $_POST['player_three_name'];
        $player_three_id = $_POST['player_three_id'];
        $player_four = $_POST['player_four_id'];
        $player_four_id = $_POST['player_four_id'];
    }

    $insert = "INSERT INTO  user_team_info(team_leader, role, team_name, game_id, game_type, team_type, team_image) 
    VALUES ('$username','$role','$team_name','$game_id','$game_type','$team_size','$folder')";

    if ($connect->query($insert)) {
        echo '<script> alert("Data Submitted"); </script>';
    } else {
        echo $connect->error; 
    }

    // dependencies insertion
    if ($team_size == 'duo') {
        $insert = "INSERT INTO team_player_info(team_name, player_one,player_one_id,player_two,player_two_id) 
        VALUES ('$team_name','$player_one','$player_one_id','$player_two','$player_two_id')";
    } else if ($team_size == 'squad') {
        $insert = "INSERT INTO team_player_info(team_name, player_one,player_one_id,player_two,player_two_id,player_three,player_three_id,player_four,player_four_id) 
        VALUES ('$team_name','$player_one','$player_one_id','$player_two','$player_two_id','$player_three','$player_three_id','$player_four','$player_four_id')";
    }

    if ($connect->query($insert)) {
        echo '<script> alert("Data Submitted"); </script>';
    } else {
        echo $connect->error;
    }
}

?>
<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Document</title>
    <link rel="stylesheet" href="../css/bootstrap.css">
    <script src="../Jquery/jquery-3.4.1.js"></script>
    <script src="../js/bootstrap.js"></script>
    <script src="../Font/fontawesome-free-5.12.0-web/css/all.css"></script>
    <script src="https://kit.fontawesome.com/a076d05399.js"></script>
    <link rel="stylesheet" type="text/css" href="gameteam1.css">
    <link rel="stylesheet" href="../static_css/header.css">
    <link rel="stylesheet" href="../static_css/foot.css">
    <link rel="stylesheet" href="../static_css/hamburger.css">
</head>

<body>

    <!-- Navbar -->
    <?php
    include 'user_header.php';
    ?>

    <!-- Navbar -->
    <div class="container-fluid position-relative">
        <div class="row">
            <div class="col-md-12 rat">

            </div>
            <!-- Profile Picture -->
            <div class="col-md-12  fat">
                <div class="center button" onmouseover="button()">
                    <img alt="person image" src="../images/profile.jpg">
                </div>

                <div class="first center popout">
                    <img src="https://img.icons8.com/dotty/50/000000/edit-property.png" /> </div>

                <div class="second center popout">
                    <img src="https://img.icons8.com/dotty/80/000000/edit-image.png" /> </div>

                <div class="third center popout">
                    <img src="https://img.icons8.com/cute-clipart/64/000000/calendar.png" />
                </div>

                <div class="fourth center popout">
                    <img src="https://img.icons8.com/dusk/64/000000/bell.png" />
                </div>

                <div class="fifth center popout">
                    <img src="https://img.icons8.com/cute-clipart/64/000000/settings.png" />
                </div>
            </div>

        </div>
    </div>

    <!-- Gap DIV -->
    <div class="gap" style="height: 10vh;"></div>
    <form action="" method="post" enctype="multipart/form-data">
        <!-- Section Second User Input Field's -->
        <div class="container user_info_section">
            <div class="row">
                <!-- <div class="col-md-8"> -->
                <!-- Form Heading -->
                <div class="col-12">
                    <h2>CREATE YOUR TEAM</h2>
                    <p id='aa'>Create a Team, add your team members to compete with other teams in the tournaments
                    </p>
                </div>

                <!-- Input Field -->
                <div class="col-sm-6">
                    <h2>TEAM NAME</h2>
                    <p id='aa'>Let us know who you are.</p>
                    <input type="text" class="input_field form-control" name="team_name" placeholder="Enter Team Name">
                </div>
                <div class="col-sm-6">
                    <h2>YOUR ROLE</h2>
                    <p id='aa'>Your team will be known as this. May be changed later.</p>
                    <input type="text" class="input_field form-control" name="role" value="Captain" readonly>
                </div>
                <div class="col-sm-6">
                    <h2>GAMING PLATFORM</h2>
                    <p id='aa'>Choose a gaming platform your team is on.</p>

                    <input type="text" class="input_field form-control" name="role" value="Mobile" readonly>
                </div>

                <div class="col-sm-6">
                    <h2>GAMES</h2>
                    <p id='aa'>Choose a game your team is on</p>

                    <select class="form-control input_field" name="game_type" aria-placeholder="Select Platform" id="game_type">
                        <option value=""></option>
                        <option class="pubg">PUBG MOBILE</option>
                        <option class="cod">CALL OF DUTY</option>
                        <option class="freefire">FREE FIRE</option>
                        <option class="fortnite">FORNITE</option>
                    </select>
                </div>

                <div class="col-sm-6">
                    <h2>TEAM SIZE</h2>
                    <p id='aa'>Please select Team Size</p>
                    <select name="team_size" class="form-control input_field" id="team_size_list">
                        <option value=""></option>
                        <option value="duo">Duo</option>
                        <option value="squad">Squad</option>
                    </select>
                </div>
                <div class="col-sm-6">
                    <h2>TEAM IMAGE</h2>
                    <p id='aa'>Please select Team Image</p>
                    <input type="file" class="input_field form-control" name="team_image" placeholder="Upload Team Images">
                </div>
            </div>
        </div>


        <!-- Team Member section -->
        <section class="team_member mt-5 pt-5 pb-5 container" id="team_member_section">
            <div class="row">
                <div class="col-12">
                    <h1 class="text-dark text-left">TEAM MEMBERS</h1>
                    <p id="aa">Please enter your team member information.</p>
                </div>

                <!-- Input Field -->
                <div class="col-12" id="member_1">
                    <div class="container">
                        <div class="row">
                            <div class="col-md-4">
                                <p id="aa">Team Member</p>
                                <input type="text" class="input_field form-control" placeholder="Enter Member Name" name="player_one_name">
                            </div>
                            <div class="col-md-4">
                                <p id="aa">In Game ID</p>
                                <input type="text" class="input_field form-control" placeholder="Enter Member Game ID" name="player_one_id">
                            </div>
                        </div>
                    </div>
                </div>

                <!-- 2nd row -->
                <div class="col-12 mt-4" id="member_2">
                    <div class="container">
                        <div class="row">
                            <div class="col-md-4">
                                <p id="aa" class="d-block d-lg-none">Team Member</p>
                                <input type="text" class="input_field form-control" placeholder="Enter Member Name" name="player_two_name">
                            </div>
                            <div class="col-md-4">
                                <p id="aa" class="d-block d-lg-none sr-only">In Game ID</p>
                                <input type="text" class="input_field form-control" placeholder="Enter Member Game ID" name="player_two_id">
                            </div>
                        </div>
                    </div>
                </div>
                <!-- 3rd row -->
                <div class="col-12 mt-4" id="member_3">
                    <div class="container">
                        <div class="row">
                            <div class="col-md-4">
                                <p id="aa" class="d-block d-lg-none">Team Member</p>
                                <input type="text" class="input_field form-control" placeholder="Enter Member Name" name="player_three_name">
                            </div>
                            <div class="col-md-4">
                                <p id="aa" class="d-block d-lg-none sr-only">In Game ID</p>
                                <input type="text" class="input_field form-control" placeholder="Enter Member Game ID" name="player_three_id">
                            </div>
                        </div>
                    </div>
                </div>

                <!-- 4th row -->
                <div class="col-12 mt-4" id="member_4">
                    <div class="container">
                        <div class="row">
                            <div class="col-md-4">
                                <p id="aa" class="d-block d-lg-none">Team Member</p>
                                <input type="text" class="input_field form-control" placeholder="Enter Member Name" name="player_four_name">
                            </div>
                            <div class="col-md-4">
                                <p id="aa" class="d-block d-lg-none sr-only">In Game ID</p>
                                <input type="text" class="input_field form-control" placeholder="Enter Member Game ID" name="player_four_id">
                            </div>
                        </div>
                    </div>
                </div>

                <!-- Data Sumbit Button -->
                <div class="col-md-12 mt-3">
                    <input type="submit" name="submit_team" value="CREATE TEAM" id="buton">
                </div>
            </div>
        </section>
    </form>


    <!-- Footer -->
    <?php
    include '../static/footer.php';
    ?>


    <script src="../JavaScript/Home.js"></script>
    <script>
        function button() {

            document.getElementsByClassName("button")[0].classList.toggle("spin");
        }


        $(document).ready(function() {

            $('#member_1').hide();
            $('#member_2').hide();
            $('#member_3').hide();
            $('#member_4').hide();

            $('#team_size_list').change(function() {
                let team_size = $('#team_size_list option:selected').text();

                if (team_size == 'Duo') {
                    $('#member_1').show();
                    $('#member_2').show();
                    $('#member_3').hide();
                    $('#member_4').hide();
                } else if (team_size == 'Squad') {
                    $('#member_1').show();
                    $('#member_2').show();
                    $('#member_3').show();
                    $('#member_4').show();

                } else {
                    $('#member_1').hide();
                    $('#member_2').hide();
                    $('#member_3').hide();
                    $('#member_4').hide();
                }
            });
        });
    </script>
</body>

</html>