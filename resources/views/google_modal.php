

    <!-- google model -->
    <div class="modal fade" id="google_modal" tabindex="-1" role="dialog" style="z-index: 11111;" aria-labelledby="exampleModalLabel" aria-hidden="true">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title" id="exampleModalLabel">Complete Your Credential Form</h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <div class="modal-body">
                    <form action="" method="post" class="form">
                        <p class="">or use your email for registeration</p>
                        <div class="form-group">
                            <label for="">Username</label>
                            <input type="text" placeholder="gagan007soul" class="form-control" id="username" pattern="^\S+$" autocomplete="true">

                        </div>
                        <div class="form-group">
                            <label for="">Email address</label>
                            <input type="email" placeholder="name@example.com" value="<?php echo $_SESSION['user_email_address']; ?>" class="form-control" id="email" autocomplete="true" readonly>
                            <small id="emailHelp" class="form-text text-muted">We'll never share your email with anyone else.</small>
                        </div>
                        <div class="form-group">
                            <label for="">Password</label>
                            <input type="password" placeholder="Password" class="form-control" id="password" title="Must contain at least one number and one uppercase and lowercase letter, and at least 8 or more characters" pattern="(?=.*\d)(?=.*[a-z])(?=.*[A-Z]).{8,}" autocomplete="true">

                        </div>
                        <div class="form-group">
                            <label for="">Which type you wanna choose.</label>
                            <select name="" id="role" class="form-control">
                                <option value="user">User</option>
                                <option value="organizer">Organizer</option>
                            </select>
                        </div>
                        <button type="submit" class="btn btn-secondary" id="registeration">Sign Up</button>
                        <button type="buttom" class="btn btn-danger" id="close">close</button>


                    </form>
                </div>
            </div>
        </div>
    </div>
    
    <?php
    include 'footer.php';
    ?>
    <?php
    include '../login_modal.php'
    ?>

    <!-- <script src="../login.js"></script> -->
    <script src="../JavaScript/Home.js"></script>

    <script>
        function gmail(email) {

            if (email.endsWith('@gmail.com') == true || email.endsWith('@yahoo.com') == true) {
                return 'true';
            } else {
                return 'false';
            }
        }

        var myInput = document.getElementById("password");

        myInput.onkeyup = function() {
            // Validate lowercase letters
            var lowerCaseLetters = /[a-z]/g;
            if (myInput.value.match(lowerCaseLetters)) {
                letter.classList.remove("invalid");
                letter.classList.add("valid");
            } else {
                letter.classList.remove("valid");
                letter.classList.add("invalid");
            }

            // Validate capital letters
            var upperCaseLetters = /[A-Z]/g;
            if (myInput.value.match(upperCaseLetters)) {
                capital.classList.remove("invalid");
                capital.classList.add("valid");
            } else {
                capital.classList.remove("valid");
                capital.classList.add("invalid");
            }

            // Validate numbers
            var numbers = /[0-9]/g;
            if (myInput.value.match(numbers)) {
                number.classList.remove("invalid");
                number.classList.add("valid");
            } else {
                number.classList.remove("valid");
                number.classList.add("invalid");
            }

            // Validate length
            if (myInput.value.length >= 8) {
                length.classList.remove("invalid");
                length.classList.add("valid");
            } else {
                length.classList.remove("valid");
                length.classList.add("invalid");
            }
        }

       

    </script>

</body>

</html>